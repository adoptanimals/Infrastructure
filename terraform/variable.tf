variable "do_region" {
    default = "lon1"
}

variable "do_image" {
    default = "ubuntu-16-04-x64"
}