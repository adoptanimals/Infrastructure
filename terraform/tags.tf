resource "digitalocean_tag" "front" {
  name = "front"
}

resource "digitalocean_tag" "docker" {
  name = "docker"
}

resource "digitalocean_tag" "database" {
  name = "database"
}

resource "digitalocean_tag" "consul" {
  name = "consul"
}