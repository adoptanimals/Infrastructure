resource "digitalocean_droplet" "docker-1" {
    name = "docker-1"

    image = "${var.do_image}"
    region = "${var.do_region}"
    size = "${local.do_small}"

    tags = ["${digitalocean_tag.docker.id}", "${digitalocean_tag.consul.id}"]

    private_networking = true
    monitoring = true
    ipv6 = true
    
    ssh_keys = [
        "${var.ssh_fingerprint}"
    ]

    connection {
        user = "root"
        type = "ssh"
        agent = true
        agent_identity = "${var.agent_identity}"
        timeout = "2m"
    }

    provisioner "remote-exec" {
        inline = [
            "echo 'Resource up!'"
        ]
    }
}    