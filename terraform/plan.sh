#!/bin/bash

: "${DO_API_TOKEN?Need to set DO_API_TOKEN}"
: "${AGENT_IDENTITY?Need to set AGENT_IDENTITY}"
: "${SSH_FINGERPRINT?Need to set SSH_FINGERPRINT}"

terraform plan -var "do_token=${DO_API_TOKEN}" -var "agent_identity=${AGENT_IDENTITY}" -var "ssh_fingerprint=${SSH_FINGERPRINT}"