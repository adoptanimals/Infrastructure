#!/bin/bash
set -e 
set -o pipefail

: "${DO_API_TOKEN?Need to set DO_API_TOKEN}"
: "${PRIVATE_KEY?Need to set PRIVATE_KEY}"
: "${CLOUDFLARE_TOKEN?Need to set CLOUDFLARE_TOKEN}"
: "${CLOUDFLARE_EMAIL?Need to set CLOUDFLARE_EMAIL}"
: "${CONSUL_ENCRYPT_KEY?Need to set CONSUL_ENCRYPT_KEY}"

export ANSIBLE_HOST_KEY_CHECKING=False

ansible-playbook -u root \
    --private-key "${PRIVATE_KEY}" \
    --inventory-file=digital_ocean.py \
    -e 'ansible_python_interpreter=/usr/bin/python3' \
    databases.yaml